class Timer

  def initialize
    @seconds = 0
  end

  def seconds
    @seconds
  end

  def minutes
    @minutes = @seconds/60
    @seconds = @seconds - 60*@minutes
  end

  def hours
    @hours = @minutes/60
    @minutes = @minutes - 60*@hours
  end

  def seconds=(seconds)
    @seconds = seconds
  end

  def time_string
    minutes
    hours
    "#{padded(@hours)}:#{padded(@minutes)}:#{padded(@seconds)}"
  end

  def padded(measure)
    if measure.to_s.length == 1
      measure = "0#{measure}"
    else
      measure = "#{measure}"
    end
  end
end
