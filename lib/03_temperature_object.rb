class Temperature

  def initialize(options_hash = {})
    @fahrenheit = options_hash[:f]
    @celsius = options_hash[:c]
  end

  def ftoc
    @celsius = (@fahrenheit - 32.0)*(5.0/9.0)
  end

  def ctof
    @fahrenheit = @celsius*(9.0/5.0) + 32.0
  end

  def in_celsius
    return @celsius unless @celsius.nil?
    ftoc if @celsius.nil?
  end

  def in_fahrenheit
    return @fahrenheit unless @fahrenheit.nil?
    ctof if @fahrenheit.nil?
  end

  def self.from_celsius(temp)
    Temperature.new({:c => temp})
  end

  def self.from_fahrenheit(temp)
    Temperature.new({:f => temp})
  end
end

class Celsius < Temperature
  def initialize (temp)
    @celsius = temp
  end
end

class Fahrenheit < Temperature
  def initialize (temp)
    @fahrenheit = temp
  end
end
