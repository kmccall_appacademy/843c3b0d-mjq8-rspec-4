class Dictionary

  attr_reader :entries

  def initialize
    @entries = {}
  end
  def entries
    @entries
  end
  def add(entry)
    @entries = @entries.merge(entry) if entry.is_a?(Hash)
    @entries[entry] = nil if entry.is_a?(String)
  end
  def keywords
    @entries.keys.sort
  end

  def include?(entry)
    @entries.keys.include?(entry)
  end

  def find(search)
    result = {}
    @entries.each_key do |key|
      result[key] = @entries[key] if key.include?(search)
    end
    result
  end

  def printable
    result = ""
    @entries.keys.sort.each do |key|
      result << "[#{key}] \"#{@entries[key]}\"\n"
    end
    result[0...-2] << "\""
  end
end
