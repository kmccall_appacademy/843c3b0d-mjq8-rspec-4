class Book

attr_accessor  :title

  def title=(title)
    result = []
    lower_list = ["a", "the", "an", "and", "in", "of"]
    title.split.each do |word|
      result << word.capitalize unless lower_list.include?(word)
      result << word if lower_list.include?(word)
    end
    result[0] = result[0].capitalize
    @title = result.join(" ")
  end

end
